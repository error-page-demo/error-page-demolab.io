Demo to show `/404.html` not being served even though it exists.

See [issue #183](https://gitlab.com/gitlab-org/gitlab-pages/issues/183) for the latest details.